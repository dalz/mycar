ChaseCamera = function() {
    this.car_frame = glMatrix.mat4.create();

    this.update = (car_frame) =>
        glMatrix.mat4.invert(this.car_frame, car_frame);

    this.matrix = () => {
        let m = glMatrix.mat4.create();
        glMatrix.mat4.lookAt(m, [0, 3, 10], [0, 4, -7], [0, 1, 0]);
        return glMatrix.mat4.multiply(m, m, this.car_frame);
    }
}

RotatingChaseCamera = function() {
  let angle = 0;
  let cfinv = glMatrix.mat4.create();

  this.update = (car_frame) => {
    glMatrix.mat4.invert(cfinv, car_frame);
    angle += 0.001;
  }

  this.matrix = () => {
    let m = glMatrix.mat4.create();
    let eye = glMatrix.vec3.fromValues(0, 3, 10);
    let center = [0, 1, 0];
    glMatrix.vec3.rotateY(eye, eye, center, angle);
    glMatrix.mat4.lookAt(m, eye, center, [0, 1, 0]);
    return glMatrix.mat4.multiply(m, m, cfinv);
  }
}

/*
the FollowFromUpCamera always look at the car from a position abova right over the car
*/
FollowFromUpCamera = function(){

  /* the only data it needs is the position of the camera */
  this.pos = [0,0,0];
  
  /* update the camera with the current car position */
  this.update = function(car_frame){
    this.pos = car_frame.slice(12, 15);
  }

  /* return the transformation matrix to transform from worlod coordiantes to the view reference frame */
  this.matrix = function(){
    return glMatrix.mat4.lookAt(glMatrix.mat4.create(),[ this.pos[0],50, this.pos[2]], this.pos,[0, 0, -1]);	
  }
}

/* the main object to be implementd */
var Renderer = new Object();

/* array of cameras that will be used */
Renderer.cameras = [];
Renderer.cameras.push(new FollowFromUpCamera());
Renderer.cameras.push(new ChaseCamera());
Renderer.cameras.push(new RotatingChaseCamera());
// set the camera currently in use
Renderer.currentCamera = 1;

/*
create the buffers for an object as specified in common/shapes/triangle.js
*/
Renderer.createObjectBuffers = function (gl, obj) {

  obj.vertexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, obj.vertexBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, obj.vertices, gl.STATIC_DRAW);
  gl.bindBuffer(gl.ARRAY_BUFFER, null);

  obj.indexBufferTriangles = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, obj.indexBufferTriangles);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, obj.triangleIndices, gl.STATIC_DRAW);
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

  obj.normalBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, obj.normalBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, obj.normals, gl.STATIC_DRAW);
  gl.bindBuffer(gl.ARRAY_BUFFER, null);

  // create edges
  var edges = new Uint16Array(obj.numTriangles * 3 * 2);
  for (var i = 0; i < obj.numTriangles; ++i) {
    edges[i * 6 + 0] = obj.triangleIndices[i * 3 + 0];
    edges[i * 6 + 1] = obj.triangleIndices[i * 3 + 1];
    edges[i * 6 + 2] = obj.triangleIndices[i * 3 + 0];
    edges[i * 6 + 3] = obj.triangleIndices[i * 3 + 2];
    edges[i * 6 + 4] = obj.triangleIndices[i * 3 + 1];
    edges[i * 6 + 5] = obj.triangleIndices[i * 3 + 2];
  }

  obj.indexBufferEdges = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, obj.indexBufferEdges);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, edges, gl.STATIC_DRAW);
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
};

/*
draw an object as specified in common/shapes/triangle.js for which the buffer 
have alrady been created
*/
Renderer.drawObject = function (gl, obj, fillColor, lineColor, isSpec) {
  gl.uniform1i(this.shader.uIsSpecLocation, isSpec);

  gl.bindBuffer(gl.ARRAY_BUFFER, obj.normalBuffer);
  gl.enableVertexAttribArray(this.shader.aNormalIndex);
  gl.vertexAttribPointer(this.shader.aNormalIndex, 3, gl.FLOAT, false, 0, 0);

  gl.bindBuffer(gl.ARRAY_BUFFER, obj.vertexBuffer);
  gl.enableVertexAttribArray(this.shader.aPositionIndex);
  gl.vertexAttribPointer(this.shader.aPositionIndex, 3, gl.FLOAT, false, 0, 0);

  gl.enable(gl.POLYGON_OFFSET_FILL);
  gl.polygonOffset(1.0, 1.0);

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, obj.indexBufferTriangles || obj.indexBuffer);
  gl.uniform4fv(this.shader.uColorLocation, fillColor);
  gl.drawElements(gl.TRIANGLES, obj.triangleIndices.length, gl.UNSIGNED_SHORT, 0);

  gl.disable(gl.POLYGON_OFFSET_FILL);

  if (obj.indexBufferEdges) {
    gl.uniform4fv(this.shader.uColorLocation, lineColor);
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, obj.indexBufferEdges);
    gl.drawElements(gl.LINES, obj.numTriangles * 3 * 2, gl.UNSIGNED_SHORT, 0);
  }

  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
  gl.disableVertexAttribArray(this.shader.aPositionIndex);
  gl.bindBuffer(gl.ARRAY_BUFFER, null);
};

/* move wheel down so that it rotates around the x axis */
function translate_wheel() {
  let pos = wheel.vertices[0].values;
  let max = -Infinity;
  let min = +Infinity;

  for (let i = 1; i < pos.length; i += 3) {
    max = Math.max(max, pos[i]);
    min = Math.min(min, pos[i]);
  }

  let off = (max - min) / 2;

  for (let i = 1; i < pos.length; i += 3)
    pos[i] -= off;
}

/*
initialize the object in the scene
*/
Renderer.initializeObjects = function (gl) {
  Game.setScene(scene_0);
  this.car = Game.addCar("mycar");

  this.cube = new Cube();
  this.cylinder = new Cylinder(10);
  translate_wheel();
  this.wheel = loadOnGPU(gl, wheel);
  this.teapot = loadOnGPU(gl, teapot);
  ComputeNormals(this.cube);
  ComputeNormals(this.cylinder);

  Renderer.createObjectBuffers(gl, this.cube);
  Renderer.createObjectBuffers(gl, this.cylinder);

  ComputeNormals(Game.scene.trackObj);
  Renderer.createObjectBuffers(gl,Game.scene.trackObj);

  ComputeNormals(Game.scene.groundObj);
  Renderer.createObjectBuffers(gl,Game.scene.groundObj);

  for (var i = 0; i < Game.scene.buildings.length; ++i) {
    ComputeNormals(Game.scene.buildingsObj[i]);
    Renderer.createObjectBuffers(gl,Game.scene.buildingsObj[i]);
  }
};

Renderer.bindModelViewMatrix = function (gl, stack) {
  gl.uniformMatrix4fv(this.shader.uModelViewMatrixLocation, false, stack.matrix);
  gl.uniformMatrix4fv(this.shader.uNormalMatrixLocation, false, stack.inverseTranspose);
}


/*
draw the car
*/
Renderer.drawCar = function (gl, stack) {
    let draw = (obj, color) =>
        this.drawObject(gl, obj, [...color, 1], [...color, 1], true);

    let m = glMatrix.mat4.create();
    let q = glMatrix.quat.create();

    stack.push();

    stack.multiply(glMatrix.mat4.fromTranslation(m, [0, .5, 0]))
    this.bindModelViewMatrix(gl, stack);
    draw(this.teapot, [1, 0, 0]);

    stack.push();

    let wm = glMatrix.mat4.create();
    let wp = [1, 0, 0.8];
    let ws = [1, 1, 1];
    let wa = 100 * this.car.wheelsAngle;

    this.car.wrot = (this.car.wrot || 0) - 4 * this.car.speed / ws[0];

    stack.multiply(glMatrix.mat4.fromRotationTranslationScale(wm,
        glMatrix.quat.fromEuler(q, this.car.wrot, 0, 0), wp, ws));

    this.bindModelViewMatrix(gl, stack);
    draw(this.wheel, [0, 0, 0]);

    stack.pop();
    stack.push();

    stack.multiply(glMatrix.mat4.fromScaling(m, [-1, 1, 1]));
    stack.multiply(wm);

    this.bindModelViewMatrix(gl, stack);
    draw(this.wheel, [0, 0, 0]);

    stack.pop();
    stack.push();

    wp[2] = -wp[2];
    stack.multiply(glMatrix.mat4.fromRotationTranslationScale(wm,
        glMatrix.quat.fromEuler(q, this.car.wrot, wa, 0), wp, ws));

    this.bindModelViewMatrix(gl, stack);
    draw(this.wheel, [0, 0, 0]);

    stack.pop();
    stack.push();

    stack.multiply(glMatrix.mat4.fromScaling(m, [-1, 1, 1]));
    stack.multiply(glMatrix.mat4.fromRotationTranslationScale(wm,
        glMatrix.quat.fromEuler(q, this.car.wrot, -wa, 0), wp, ws));

    this.bindModelViewMatrix(gl, stack);
    draw(this.wheel, [0, 0, 0]);

    stack.pop();
    stack.pop();
};

Renderer.drawLamp = function (gl, stack, lamp) {
  let m = glMatrix.mat4.create();
  stack.push();
  stack.multiply(glMatrix.mat4.fromTranslation(m, lamp.position));
  stack.multiply(glMatrix.mat4.fromScaling(m, [0.1, lamp.height / 2, 0.1]));
  this.bindModelViewMatrix(gl, stack);

  const c = [100, 100, 100, 1];
  this.drawObject(gl, this.cylinder, c, c);

  stack.pop();
}

Renderer.drawScene = function (gl) {

  var width = this.canvas.width;
  var height = this.canvas.height
  var ratio = width / height;
  var stack = new MatrixStack();

  gl.viewport(0, 0, width, height);
  
  gl.enable(gl.DEPTH_TEST);

  // Clear the framebuffer
  gl.clearColor(0.34, 0.5, 0.74, 1.0);
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);


  gl.useProgram(this.shader);

  gl.uniformMatrix4fv(this.shader.uProjectionMatrixLocation,false,glMatrix.mat4.perspective(glMatrix.mat4.create(),3.14 / 4, ratio, 1, 500));

  Renderer.cameras[Renderer.currentCamera].update(this.car.frame);
  var invV = Renderer.cameras[Renderer.currentCamera].matrix();
  gl.uniformMatrix4fv(this.shader.uViewMatrixLocation, false, invV);

  gl.uniform3fv(this.shader.uSunDirectionLocation, Game.scene.weather.sunLightDirection);

  // initialize the stack with the identity
  stack.loadIdentity();
  // multiply by the view matrix
  stack.multiply(invV);

  // drawing the car
  stack.push();

  stack.multiply(this.car.frame);
  this.drawCar(gl, stack);
  stack.pop();

  this.bindModelViewMatrix(gl, stack);

  // drawing the static elements (ground, track and buldings)
  this.drawObject(gl, Game.scene.groundObj, [0.3, 0.7, 0.2, 1.0], [0, 0, 0, 1.0]);
  this.drawObject(gl, Game.scene.trackObj, [0.9, 0.8, 0.7, 1.0], [0, 0, 0, 1.0]);
  for (var i in Game.scene.buildingsObj) 
    this.drawObject(gl, Game.scene.buildingsObj[i], [0.8, 0.8, 0.8, 1.0], [0.2, 0.2, 0.2, 1.0]);

  let lpos = new Float32Array(Game.scene.lamps.length * 3);
  for (let i in Game.scene.lamps) {
    let l = Game.scene.lamps[i];

    [lpos[3*i], lpos[3*i+1], lpos[3*i+2]] = l.position;
    lpos[3*i+1] += l.height;

    this.drawLamp(gl, stack, l);
  }
  gl.uniform3fv(this.shader.uLampPositionLocation, lpos);

  gl.useProgram(null);
};



Renderer.Display = function () {
  Renderer.drawScene(Renderer.gl);
  window.requestAnimationFrame(Renderer.Display) ;
};


Renderer.setupAndStart = function () {
 /* create the canvas */
	Renderer.canvas = document.getElementById("OUTPUT-CANVAS");
  
 /* get the webgl context */
	Renderer.gl = Renderer.canvas.getContext("webgl");

  /* read the webgl version and log */
	var gl_version = Renderer.gl.getParameter(Renderer.gl.VERSION); 
	log("glversion: " + gl_version);
	var GLSL_version = Renderer.gl.getParameter(Renderer.gl.SHADING_LANGUAGE_VERSION)
	log("glsl  version: "+GLSL_version);

  /* create the matrix stack */
	Renderer.stack = new MatrixStack();

  /* initialize objects to be rendered */
  Renderer.initializeObjects(Renderer.gl);

  /* create the shader */
  Renderer.shader = new Shader(Renderer.gl);

  /*
  add listeners for the mouse / keyboard events
  */
  Renderer.canvas.addEventListener('mousemove',on_mouseMove,false);
  Renderer.canvas.addEventListener('keydown',on_keydown,false);
  Renderer.canvas.addEventListener('keyup',on_keyup,false);

  Renderer.Display();
}

on_mouseMove = function(e){}

on_keyup = function(e){
	Renderer.car.control_keys[e.key] = false;
}
on_keydown = function(e){
	Renderer.car.control_keys[e.key] = true;
}

window.onload = Renderer.setupAndStart;



